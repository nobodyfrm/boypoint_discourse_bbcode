import { registerOption } from "pretty-text/pretty-text";

registerOption((siteSettings, opts) => {
  opts.features["bbcode-boypoint"] = true;
});

export function setup(helper) {
  helper.whiteList({
    custom(tag, name, value) {
      if (tag === "span" && name === "style") {
        return true;
      }
    }
  });

  if (helper.markdownIt) {
    helper.registerPlugin(md => {
      const ruler = md.inline.bbcode.ruler;

      ruler.push("background", {
        tag: "background",
        wrap: function(startToken, endToken, tagInfo, content) {
          const background = (tagInfo.attrs["_default"] || content).trim();

          startToken.type = "span_open";
          startToken.tag = "span";
          startToken.attrs = [
            ["style", "background-color:"+background+";"]
          ];
          startToken.content = "";
          startToken.nesting = 1;

          endToken.type = "span_close";
          endToken.tag = "span";
          endToken.content = "";
          endToken.nesting = -1;

          return false;
        }
      });
      ruler.push("lush", {
        tag: "lush",
        wrap: function(token, endToken, tagInfo, content) {
          token.type = "span_open";
          token.tag = "span";
          token.attrs = [
            ["style", "color: #a40f20"]
          ];
          token.content = "";
          token.nesting = 1;

          endToken.type = "span_close";
          endToken.tag = "span";
          endToken.nesting = -1;
          endToken.content = "";
        }
      });
      ruler.push("marketing", {
        tag: "marketing",
        wrap: function(token, endToken, tagInfo, content) {
          token.type = "span_open";
          token.tag = "span";
          token.attrs = [
            ["style", "color: #99618b; font-weight: bold"]
          ];
          token.content = "";
          token.nesting = 1;

          endToken.type = "span_close";
          endToken.tag = "span";
          endToken.nesting = -1;
          endToken.content = "";
        }
      });
      ruler.push("mod", {
        tag: "mod",
        wrap: function(token, endToken, tagInfo, content) {
          token.type = "span_open";
          token.tag = "span";
          token.attrs = [
            ["style", "color:#8FC434;font-weight:bold;"]
          ];
          token.content = "";
          token.nesting = 1;

          endToken.type = "span_close";
          endToken.tag = "span";
          endToken.nesting = -1;
          endToken.content = "";
        }
      });
      ruler.push("ot", {
        tag: "ot",
        replace: function(state, tagInfo, content) {
          console.log(state);
          let token;
          token = state.push("html_raw", "", 0);
          token.content = '<p style="margin-bottom: 0px;"><span style="color: #C2C2C2;">Offtopic:</span></p><p style="padding: 1em; margin-top: 0px; border: 1px dotted #C2C2C2; font-size: 0.9em; font-weight: normal; color: #C2C2C2 !important;">'+content+'</p>';
          return true;
        }
      });
      ruler.push("qzl", {
        tag: "qzl",
        wrap: function(token, endToken, tagInfo, content) {
          token.type = "span_open";
          token.tag = "span";
          token.attrs = [
            ["style", "color: #4997d0; font-style: italic"]
          ];
          token.content = "";
          token.nesting = 1;

          endToken.type = "span_close";
          endToken.tag = "span";
          endToken.nesting = -1;
          endToken.content = "";
        }
      });
      ruler.push("sup", {
        tag: "sup",
        wrap: function(token, endToken, tagInfo, content) {
          token.type = "sup_open";
          token.tag = "sup";
          token.content = "";
          token.nesting = 1;

          endToken.type = "sup_close";
          endToken.tag = "sup";
          endToken.nesting = -1;
          endToken.content = "";
        }
      });

    });
  } else {
    helper.addPreProcessor(text => "Please update your Discourse!");
  }
}
